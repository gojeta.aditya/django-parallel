import fileinput
import os
import shlex
import subprocess
import sys
from pathlib import Path

from semantic_version import Version
from datetime import date

from parallelize import __version__


release_identifier_to_handler = {
    "major": "next_major",
    "minor": "next_minor",
    "patch": "next_patch"
}


def run_cmd(cmd):
    try:
        subprocess.run(
            shlex.split(cmd),
            capture_output=True,
            check=True
        )
    except subprocess.CalledProcessError as e:
        err_dict = {
            "code": e.returncode,
            "stdout": e.stdout,
            "stderr": e.stderr,
        }
        print("error is ", err_dict)
        raise e


def get_next_build_version(sem_ver, release_identifier, commit_sha):
    handler = getattr(sem_ver, release_identifier_to_handler[release_identifier])
    next_sem_ver = (
            str(handler())
            + "+build-"
            + str(date.today())
            + "-" + commit_sha
    )
    print("next version is ", next_sem_ver)
    return next_sem_ver



def get_next_stable_version(sem_ver):
    return str(sem_ver.truncate("prerelease"))


def write_version(latest_version):
    file_path = str(
        Path(__file__).resolve().parent.joinpath("parallelize", "__init__.py")
    )
    version_line = "__version__ = \"{latest_version}\"\n".format(
        latest_version=latest_version
    )
    with fileinput.input(files=file_path, inplace=True) as f:
        for line in f:
            if "__version__" in line:
                sys.stdout.write(line.replace(line, version_line))

if __name__ == "__main__":
    branch_name = os.getenv("CI_COMMIT_REF_NAME")
    release_identifier = sys.argv[1]
    short_sha = os.getenv("CI_COMMIT_SHORT_SHA")

    print(branch_name, release_identifier)

    sem_ver = Version(__version__)
    latest_version = None

    if branch_name == "develop" or branch_name.startswith("staging/"):
        latest_version = get_next_build_version(
            sem_ver, release_identifier, short_sha
        )
    elif branch_name == "master" or branch_name.startswith("maint/"):
        latest_version = get_next_stable_version(sem_ver)

    write_version(latest_version)
