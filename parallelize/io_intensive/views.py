import concurrent.futures
# Create your views here.
import time

from rest_framework.response import Response
from rest_framework.views import APIView
from statistics import mean


def load_url():
    import requests
    r = requests.get("https://httpbin.org/json")
    print(r.status_code)


class SerialView(APIView):
    def get(self, request):
        st_time = time.time()
        for i in range(2):
            load_url()

        end_time = time.time() - st_time

        return Response({"time_taken": end_time})


class ThreadBasedParallelView(APIView):

    def _calibrate(self):
        st_time = time.time()

        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            # Start the load operations and mark each future with its URL

            future_list = [
                executor.submit(
                    load_url
                ) for _ in range(4)
            ]

            # future_to_url = {executor.submit(load_url, url, 60): url for url in URLS}
            for index, future in enumerate(concurrent.futures.as_completed(future_list)):

                try:
                    print("job ", index, "succeeded")

                except Exception as exc:
                    print('generated an exception: ', exc, str(index))

            time_diff = time.time() - st_time

        return time_diff

    def get(self, request):

        average_time = mean([
            self._calibrate() for _ in range(10)
        ])

        return Response({"time_taken": average_time})

class ProcessBasedParallelView(APIView):

    def _calibrate(self):
        st_time = time.time()

        with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
            # Start the load operations and mark each future with its URL

            future_list = [
                executor.submit(
                    load_url
                ) for _ in range(4)
            ]

            # future_to_url = {executor.submit(load_url, url, 60): url for url in URLS}
            for index, future in enumerate(concurrent.futures.as_completed(future_list)):

                try:
                    print("job ", index, "succeeded")

                except Exception as exc:
                    print('generated an exception: ', exc, str(index))

            time_diff = time.time() - st_time

        return time_diff

    def get(self, request):
        average_time = mean([
            self._calibrate() for _ in range(10)
        ])

        return Response({"time_taken": average_time})

