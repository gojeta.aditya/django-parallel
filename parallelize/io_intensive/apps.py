from django.apps import AppConfig
from django.db import connections


class ThreadBasedConfig(AppConfig):
    name = 'io_intensive'

    def ready(self):
        print("within ready...")
        connections.databases.clear()
        print("connections.databases is ", connections.databases)
