import requests

def serial_io():

    r = requests.get(
        "http://localhost:8500/api/io-serial/"
    )

    print(r.json())

def thread_parallel_io():

    r = requests.get(
        "http://localhost:8000/api/io-thread-parallel/"
    )

    print(r.json())

def process_parallel_io():

    r = requests.get(
        "http://localhost:8000/api/io-process-parallel/"
    )

    print(r.json())

serial_io()
# thread_parallel_io()
# process_parallel_io()


# {'time_taken': 0.8691732168197632} -> thread parallel
# {'time_taken': 0.8767992496490479} -> process parallel
