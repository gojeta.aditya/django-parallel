import subprocess
import shlex
import os
from parallelize import __version__

git_username_cmd = "git config --global user.name \"{name}\""
git_email_cmd = "git config --global user.email \"{email}\""
git_remote_add = "git remote set-url origin {origin_url}"

git_add_version_file = "git add ./parallelize/__init__.py"
git_commit_version_file = "git commit -m 'Updated version file [skip ci]'"

git_push_version_file = (
    "git push origin {source_branch_name}"
)

def run_cmd(cmd):
    try:
        subprocess.run(
            shlex.split(cmd),
            capture_output=True,
            check=True
        )
    except subprocess.CalledProcessError as e:
        err_dict = {
            "code": e.returncode,
            "stdout": e.stdout,
            "stderr": e.stderr,
        }
        print("error is ", err_dict)
        raise e



def set_git_context():
    author = os.getenv("CI_COMMIT_AUTHOR")
    print("author is ", author)
    name, email = author.replace("<", '').replace(">", '').rsplit(maxsplit=1)
    run_cmd(git_username_cmd.format(name=name))
    run_cmd(git_email_cmd.format(email=email))
    origin_url = "git@gitlab.com:{CI_PROJECT_PATH}.git".format(
        CI_PROJECT_PATH=os.getenv("CI_PROJECT_PATH")
    )
    run_cmd(
        git_remote_add.format(origin_url=origin_url)
    )


def push_version_file(source_branch_name):
    run_cmd(git_add_version_file)
    run_cmd(git_commit_version_file)
    run_cmd(git_push_version_file.format(source_branch_name=source_branch_name))


if __name__ == "__main__":
    set_git_context()
    branch_name = os.getenv("CI_COMMIT_REF_NAME")

    tag_msg = "version {branch_name}"

    if branch_name == "develop" or branch_name.startswith("staging/"):
        tag_msg = "New build version for branch {branch_name}"
    elif branch_name == "master" or branch_name.startswith("maint/"):
        tag_msg = "New stable version {branch_name}"

    tag_cmd = "git tag -a {version} -m '{tag_msg}'".format(
        version=__version__,
        tag_msg=tag_msg.format(
            branch_name=branch_name
        )
    )

    push_cmd = "git push -o ci.skip origin --tags"

    push_version_file(branch_name)
    run_cmd(tag_cmd)
    run_cmd(push_cmd)
