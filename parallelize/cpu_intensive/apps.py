from django.apps import AppConfig


class ProcessBasedConfig(AppConfig):
    name = 'cpu_intensive'
