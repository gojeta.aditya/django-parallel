import concurrent.futures
import json

import youtube_dl

def download_video(data_obj_list):
    print("within download video..")
    def my_hook(d):
        # print("d is ", d)
        if d['status'] == 'finished':
            print('Done downloading, now converting ...')

    for data_obj in data_obj_list:

        ydl_opts = {
            'format': 'best',
            'geturl': True,
            'outtmpl': '/Users/I0847/Documents/vimeo_vids/' + data_obj['filename'] + '.%(ext)s',
            'progress_hooks': [my_hook],
            'newline': True,
            'nocheckcertificate': True
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([data_obj['url']])




with concurrent.futures.ThreadPoolExecutor(max_workers=26) as executor:
    # Start the load operations and mark each future with its URL
    future_list = []

    with open('/Users/I0847/Desktop/urls.json', 'r') as f:
        data = json.load(f)
        # print("data is ", data)

    task_list = []
    mini_list = []

    for key, item in data.items():
        if item['url']:
            data_obj = {
                "filename": item['text'].replace('\n', '_').replace(' ', '_') + "_" + key,
                "url": item['url']
            }

            mini_list.append(data_obj)

            if len(mini_list) % 10 == 0:
                task_list.append(mini_list[:])
                mini_list.clear()

    if mini_list:
        task_list.append(mini_list[:])
        mini_list.clear()

    # print("task_list is ", task_list)

    for data_obj_list in task_list:
        future_list.append(
            executor.submit(download_video, data_obj_list)
        )

    for index, future in enumerate(
            concurrent.futures.as_completed(future_list)
    ):
        try:
            print("job ", index, "succeeded")

        except Exception as exc:
            print('generated an exception: ', exc, str(index))